package com.rgames.githubclient.interfaces;

import retrofit2.Retrofit;

public interface IModel {

    interface IGitHubPage{
        void buscarPagina(Retrofit retrofit, int page, ICallback callback);
    }

    interface IRepositorio{

        void buscarRespositorios(Retrofit retrofit, String criador, String repositorio, ICallback iCallback);
    }
}
