package com.rgames.githubclient.interfaces;


import com.rgames.githubclient.model.basic.GitHubPage;
import com.rgames.githubclient.model.basic.Repositorio;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface GitHubPageService {
    //public static final String URL_USER = "https://api.github.com/users/";
    public static final String BASE_URL = "https://api.github.com/";

    @Headers({
            "Accept: application/vnd.github.v3+json"
    })
    @GET("search/repositories?q=language:Java&sort=stars&")
    Call<GitHubPage> getGitHubPageJavaStars(@Query("page") int page);

    @GET("search")
    Call<GitHubPage> getGitHubPageJavaStars();

    @GET("repos/{criador}/{repositorio}/pulls")
    Call<List<Repositorio>> getRepositorioList(@Path("criador") String criador, @Path("repositorio") String repositorio);
}
