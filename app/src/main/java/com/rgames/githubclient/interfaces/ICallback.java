package com.rgames.githubclient.interfaces;

public interface ICallback {

    void onResult(Object object);
    void onError(Throwable throwable);
}
