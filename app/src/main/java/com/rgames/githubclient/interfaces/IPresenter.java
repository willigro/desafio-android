package com.rgames.githubclient.interfaces;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.RecyclerView;

import com.rgames.githubclient.model.basic.GitHubPage;
import com.rgames.githubclient.model.basic.Repositorio;
import com.rgames.githubclient.view.githubpage.AdapterRecyclerViewGitHubPage;
import com.rgames.githubclient.view.repositorio.AdapterRecyclerViewRepositorios;

import java.util.List;

import retrofit2.Retrofit;

public interface IPresenter {

    interface IGitHubPage {
        void setView(IView.IGitHubPage mView);

        void setModel(IModel.IGitHubPage model);

        void buscarPagina(Retrofit retrofit, int page, RecyclerView mRecyclerView);

        void savedInstanceState(Intent intent, Bundle savedInstanceState, RecyclerView mRecyclerView);
    }

    interface IAdapterGitHubPage {
        void onBindViewHolder(AdapterRecyclerViewGitHubPage.GitHubPageHolder holder, int position);

        void setItens(GitHubPage gitHubPage);

        int getCount();
    }

    interface IRepositorio {

        void setView(IView.IRepositorio view);

        void setModel(IModel.IRepositorio model);

        void toolbar(ActionBar supportActionBar, Drawable upArrow, String string);

        void buscarRepositorios(Retrofit retrofit, Intent intent);

        void savedInstanceState(Bundle savedInstanceState);
    }

    interface IAdapterRepositorios {

        void onBindViewHolder(AdapterRecyclerViewRepositorios.RepositorioViewHolder holder, int position);

        void setItens(List<Repositorio> list);

        int getCount();
    }

    interface ISplashScreen{
        void setView(IView.ISplashScreen view);
        void buscarPagina(Retrofit retrofit, int page, RecyclerView mRecyclerView);
    }
}
