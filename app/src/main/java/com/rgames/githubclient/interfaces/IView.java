package com.rgames.githubclient.interfaces;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;

import com.rgames.githubclient.model.basic.GitHubPage;
import com.rgames.githubclient.model.basic.Repositorio;

import java.util.List;

public interface IView {

    interface IGitHubPage {
        void listPage(GitHubPage gitHubPage, RecyclerView recyclerView);

        void initProgress();

        void finishProgress();

        void dialog(String msg);

        void semConexao();

        void inicial(GitHubPage gitHubPage);

        void inicial();

        void savedInstanceState(Bundle savedInstanceState);

    }

    interface IRepositorio {
        void list(List<Repositorio> list, String titulo);

        void initProgress();

        void finishProgress();

        void dialog(String msg);

        void semConexao();

        void inicial();

        void savedInstanceState(Bundle savedInstanceState);
    }

    interface ISplashScreen{

        void iniciarAplicacaoSemLista();

        void iniciarAplicacaoComLista(GitHubPage gitHubPage);

        void semConexao();

        void erro(String s);
    }
}
