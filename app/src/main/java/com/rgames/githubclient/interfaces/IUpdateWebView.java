package com.rgames.githubclient.interfaces;

public interface IUpdateWebView {

    void update(String url);
}
