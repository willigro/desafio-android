package com.rgames.githubclient.view.repositorio;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;

import com.rgames.githubclient.R;
import com.rgames.githubclient.interfaces.IUpdateWebView;

public class RepositorioWebViewFragment extends Fragment implements IUpdateWebView{

    private String mRepo;
    private static final String ARG = "arg";
    private WebView mWebview;
    private FrameLayout mProgressBar;

    public RepositorioWebViewFragment() {
        // Required empty public constructor
    }

    public static RepositorioWebViewFragment getInstance(String repo) {
        RepositorioWebViewFragment fragment = new RepositorioWebViewFragment();
        Bundle b = new Bundle();
        b.putString(ARG, repo);
        fragment.setArguments(b);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            mRepo = getArguments().getString(ARG);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_repositorio_web_view, container, false);
        mProgressBar = view.findViewById(R.id.progress);
        mWebview = view.findViewById(R.id.webView);
        mWebview.setWebViewClient(new WebViewClient(){

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                initProgress();
            }

            public void onPageFinished(WebView view, String url) {
                finishProgress();
            }
        });
        mWebview.loadUrl((mRepo == null || mRepo.isEmpty()) ? "https://github.com/" : mRepo);
        return view;
    }

    @Override
    public void update(String url) {
        mWebview.loadUrl(url);
    }

    private void initProgress(){
        mProgressBar.setVisibility(View.VISIBLE);
    }

    public void finishProgress(){
        mProgressBar.setVisibility(View.GONE);
    }
}
