package com.rgames.githubclient.view.githubpage;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.rgames.githubclient.R;
import com.rgames.githubclient.interfaces.IPresenter;
import com.rgames.githubclient.model.basic.GitHubPage;
import com.rgames.githubclient.presenter.githubpage.AdapterGitHubPagePresenter;

public class AdapterRecyclerViewGitHubPage extends RecyclerView.Adapter<AdapterRecyclerViewGitHubPage.GitHubPageHolder> {

    private IPresenter.IAdapterGitHubPage mPresenter;

    AdapterRecyclerViewGitHubPage(GitHubPage gitHubPage, Context context) {
        mPresenter = new AdapterGitHubPagePresenter(gitHubPage, context);
    }

    @Override
    public GitHubPageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new GitHubPageHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_recyclerview_page, parent, false));
    }

    @Override
    public void onBindViewHolder(GitHubPageHolder holder, int position) {
        mPresenter.onBindViewHolder(holder, position);
    }

    @Override
    public int getItemCount() {
        return mPresenter.getCount();
    }

    public void setItems(GitHubPage gitHubPage) {
        mPresenter.setItens(gitHubPage);
    }

    public class GitHubPageHolder extends RecyclerView.ViewHolder {

        public TextView txtNomeRepositorio, txtDescricaoRepositorio, txtNomeUsuario, txtSobrenomeUsuario, txtQuantidadeFork, txtQuantidadeEstrela;
        public ImageView imgForm, imgEstrela, imgUsuario;
        public CardView cardView;

        public GitHubPageHolder(View itemView) {
            super(itemView);
            txtNomeRepositorio = itemView.findViewById(R.id.txtNomeRepositorio);
            txtDescricaoRepositorio = itemView.findViewById(R.id.txtDescricaoRepositorio);
            txtNomeUsuario = itemView.findViewById(R.id.txtNomeUsuario);
            txtSobrenomeUsuario = itemView.findViewById(R.id.txtSobrenomeUsuario);
            txtQuantidadeEstrela = itemView.findViewById(R.id.txtQuantidadeEstrelas);
            txtQuantidadeFork = itemView.findViewById(R.id.txtQuantidadeFork);
            imgEstrela = itemView.findViewById(R.id.imgEstrelas);
            imgForm = itemView.findViewById(R.id.imgFork);
            imgUsuario = itemView.findViewById(R.id.imgUsuario);
            cardView = itemView.findViewById(R.id.cardView);
        }
    }
}
