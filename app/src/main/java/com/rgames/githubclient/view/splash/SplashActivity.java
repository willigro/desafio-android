package com.rgames.githubclient.view.splash;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.rgames.githubclient.R;
import com.rgames.githubclient.interfaces.IPresenter;
import com.rgames.githubclient.interfaces.IView;
import com.rgames.githubclient.model.basic.GitHubPage;
import com.rgames.githubclient.model.mvp.GitHubPageModel;
import com.rgames.githubclient.model.util.RetrofitUtil;
import com.rgames.githubclient.presenter.splash.SplashScreenPresenter;
import com.rgames.githubclient.view.githubpage.GitHubPaginasActivity;

public class SplashActivity extends AppCompatActivity implements IView.ISplashScreen{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        IPresenter.ISplashScreen presenter = new SplashScreenPresenter(new GitHubPageModel());
        presenter.setView(this);
        presenter.buscarPagina(RetrofitUtil.getInstance(this), 1, null);
    }

    private void animacao() {
        TextView textView = findViewById(R.id.buscando);
        textView.animate().setDuration(2000).alpha(0.0f).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                textView.setVisibility(View.GONE);
                textView.animate().setListener(null);
            }
        });
    }

    @Override
    public void iniciarAplicacaoSemLista() {
        startActivity(new Intent(SplashActivity.this, GitHubPaginasActivity.class));
        finish();
    }

    @Override
    public void iniciarAplicacaoComLista(GitHubPage gitHubPage) {
        Intent intent = new Intent(SplashActivity.this, GitHubPaginasActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable("GIT", gitHubPage);
        animacao();
        new Handler().postDelayed(() -> {
            startActivity(intent.putExtras(bundle));
            finish();
        }, 2000);
    }

    @Override
    public void semConexao() {
        //Implementar algo mais agradavel.
        erro("Sem conexão");
    }

    @Override
    public void erro(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
        finish();
    }
}
