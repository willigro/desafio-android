package com.rgames.githubclient.view.repositorio;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.rgames.githubclient.R;
import com.rgames.githubclient.interfaces.IPresenter;
import com.rgames.githubclient.interfaces.IUpdateWebView;
import com.rgames.githubclient.model.basic.Repositorio;
import com.rgames.githubclient.presenter.repositorio.AdapterRepositoriosPresenter;

import java.util.List;

public class AdapterRecyclerViewRepositorios extends RecyclerView.Adapter<AdapterRecyclerViewRepositorios.RepositorioViewHolder> {

    private IPresenter.IAdapterRepositorios mPresenter;

    AdapterRecyclerViewRepositorios(List<Repositorio> list, Context context, IUpdateWebView mIUpdateWebView) {
        mPresenter = new AdapterRepositoriosPresenter(list, context, mIUpdateWebView);
    }

    @Override
    public RepositorioViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RepositorioViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_recyclerview_repositorios, parent, false));
    }

    @Override
    public int getItemCount() {
        return mPresenter.getCount();
    }

    @Override
    public void onBindViewHolder(RepositorioViewHolder holder, int position) {
        mPresenter.onBindViewHolder(holder, position);
    }

    public void setList(List<Repositorio> list){
        mPresenter.setItens(list);
    }

    public class RepositorioViewHolder extends RecyclerView.ViewHolder {

        public TextView txtPullTitulo, txtPullDescricao, txtPullData, txtUsuarioNome, txtUsuarioSobrenome;
        public CardView cardView;
        public ImageView imgUsuario;

        public RepositorioViewHolder(View itemView) {
            super(itemView);
            txtPullTitulo = itemView.findViewById(R.id.txtTitulo);
            txtPullDescricao = itemView.findViewById(R.id.txtDescricao);
            txtPullData = itemView.findViewById(R.id.txtData);
            txtUsuarioNome = itemView.findViewById(R.id.txtNomeUsuario);
            txtUsuarioSobrenome = itemView.findViewById(R.id.txtSobrenomeUsuario);
            imgUsuario = itemView.findViewById(R.id.imgUsuario);
            cardView = itemView.findViewById(R.id.cardView);
        }
    }
}
