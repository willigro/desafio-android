package com.rgames.githubclient.view.repositorio;


import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.rgames.githubclient.R;
import com.rgames.githubclient.interfaces.IPresenter;
import com.rgames.githubclient.interfaces.IUpdateWebView;
import com.rgames.githubclient.interfaces.IView;
import com.rgames.githubclient.model.basic.Repositorio;
import com.rgames.githubclient.model.mvp.RepositoriosModel;
import com.rgames.githubclient.model.util.AlertDialogUtil;
import com.rgames.githubclient.model.util.RetrofitUtil;
import com.rgames.githubclient.presenter.repositorio.RepositoriosPresenter;

import java.util.ArrayList;
import java.util.List;

public class RepositorioListaFragment extends Fragment implements IView.IRepositorio {


    private final String CURRENT_POS = "recycler";
    private final String LIST = "list";
    private final String TIT = "tit";
    private IPresenter.IRepositorio mPresenter;
    private ProgressBar mProgressBar;
    private RecyclerView mRecyclerView;
    private List<Repositorio> mRepositorioList;
    private String mTit;
    private View mView;
    private IUpdateWebView mIUpdateWebView;

    public RepositorioListaFragment() {
        // Required empty public constructor
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putParcelable(CURRENT_POS, mRecyclerView.getLayoutManager().onSaveInstanceState());
        savedInstanceState.putParcelableArrayList(LIST, (ArrayList<? extends Parcelable>) mRepositorioList);
        savedInstanceState.putString(TIT, mTit);
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_repositorio_lista, container, false);
        mProgressBar = mView.findViewById(R.id.progress);
        initProgress();
        mPresenter = new RepositoriosPresenter();
        mPresenter.setView(this);
        mPresenter.setModel(new RepositoriosModel());
        mPresenter.savedInstanceState(savedInstanceState);

        return mView;
    }

    public void setmIUpdateWebView(IUpdateWebView mIUpdateWebView) {
        this.mIUpdateWebView = mIUpdateWebView;
    }

    private void initToolbar(String title) {
        Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.BLACK);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        mPresenter.toolbar(((AppCompatActivity) getActivity()).getSupportActionBar(), upArrow, title);
    }


    @Override
    public void list(List<Repositorio> list, String titulo) {
        initToolbar(titulo);
        mTit = titulo;
        mRepositorioList = list;
        mRecyclerView = mView.findViewById(R.id.recycler);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(new AdapterRecyclerViewRepositorios(list, getActivity(), mIUpdateWebView));
        finishProgress();
    }

    @Override
    public void initProgress() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void finishProgress() {
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void dialog(String msg) {
        AlertDialogUtil.showNeutral(getActivity(), msg);
    }

    @Override
    public void semConexao() {
        //mSemConexaoLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void inicial() {
        mPresenter.buscarRepositorios(RetrofitUtil.getInstance(getActivity()), getActivity().getIntent());
    }

    @Override
    public void savedInstanceState(Bundle savedInstanceState) {
        List<Repositorio> list = savedInstanceState.getParcelableArrayList(LIST);
        list(list, savedInstanceState.getString(TIT));
        mRecyclerView.getLayoutManager().onRestoreInstanceState(savedInstanceState.getParcelable(CURRENT_POS));
    }
}
