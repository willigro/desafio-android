package com.rgames.githubclient.view.githubpage;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import com.rgames.githubclient.R;
import com.rgames.githubclient.interfaces.IPresenter;
import com.rgames.githubclient.interfaces.IView;
import com.rgames.githubclient.model.basic.GitHubPage;
import com.rgames.githubclient.model.mvp.GitHubPageModel;
import com.rgames.githubclient.model.util.AlertDialogUtil;
import com.rgames.githubclient.model.util.RetrofitUtil;
import com.rgames.githubclient.presenter.githubpage.GitHubPagePresenter;
import com.rgames.githubclient.view.EndlessScroll;

public class GitHubPaginasActivity extends AppCompatActivity implements IView.IGitHubPage {

    private final String CURRENT_PAGE = "current_page";
    private final String CURRENT_POS = "recycler";
    private final String GIT = "git";
    private IPresenter.IGitHubPage mPresenter;
    private AdapterRecyclerViewGitHubPage mAdapter;
    private GitHubPage mGitHubPage;
    private RecyclerView mRecyclerView;
    private ProgressBar mProgress;
    private FrameLayout mSemConexaoLayout;
    private int current_page = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mProgress = findViewById(R.id.progress);
        mSemConexaoLayout = findViewById(R.id.semConexao);
        mPresenter = new GitHubPagePresenter();
        mPresenter.setView(this);
        mPresenter.setModel(new GitHubPageModel());
        mGitHubPage = new GitHubPage();
        recyclerViewGitHubPage();
        mPresenter.savedInstanceState(getIntent(), savedInstanceState, mRecyclerView);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putInt(CURRENT_PAGE, current_page);
        savedInstanceState.putParcelable(CURRENT_POS, mRecyclerView.getLayoutManager().onSaveInstanceState());
        savedInstanceState.putParcelable(GIT, mGitHubPage);
        super.onSaveInstanceState(savedInstanceState);
    }

    private void recyclerViewGitHubPage() {
        mRecyclerView = findViewById(R.id.recycler);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mAdapter = new AdapterRecyclerViewGitHubPage(mGitHubPage, this);

        EndlessScroll endlessScroll = new EndlessScroll(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView recyclerView) {
                current_page = page;
                mPresenter.buscarPagina(RetrofitUtil.getInstance(GitHubPaginasActivity.this), page, recyclerView);
            }
        };
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addOnScrollListener(endlessScroll);
    }

    @Override
    public void listPage(GitHubPage gitHubPage, RecyclerView recyclerView) {
        mGitHubPage.items.addAll(gitHubPage.items);
        final int curSize = mAdapter.getItemCount();

        recyclerView.post(() -> {
            mAdapter.notifyItemRangeInserted(curSize, mGitHubPage.items.size() - 1);
            finishProgress();
        });
    }

    @Override
    public void initProgress() {
        mProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void finishProgress() {
        mProgress.setVisibility(View.GONE);
    }

    @Override
    public void dialog(String msg) {
        AlertDialogUtil.showNeutral(this, msg);
    }

    @Override
    public void semConexao() {
        //mSemConexaoLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void inicial(GitHubPage gitHubPage) {
        mGitHubPage = gitHubPage;
        recyclerViewGitHubPage();
    }

    @Override
    public void inicial() {
        mPresenter.buscarPagina(RetrofitUtil.getInstance(this), current_page, mRecyclerView);
    }

    @Override
    public void savedInstanceState(Bundle savedInstanceState) {
        mGitHubPage = savedInstanceState.getParcelable(GIT);
        current_page = savedInstanceState.getInt(CURRENT_PAGE);
        mAdapter.setItems(mGitHubPage);
        mRecyclerView.getLayoutManager().onRestoreInstanceState(savedInstanceState.getParcelable(CURRENT_POS));
    }
}
