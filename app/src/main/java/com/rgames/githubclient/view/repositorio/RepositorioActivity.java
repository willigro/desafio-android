package com.rgames.githubclient.view.repositorio;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.rgames.githubclient.R;

public class RepositorioActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repositorio);

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            RepositorioWebViewFragment webViewFragment = new RepositorioWebViewFragment();
            RepositorioListaFragment listaFragment = new RepositorioListaFragment();
            listaFragment.setmIUpdateWebView(webViewFragment);
            getSupportFragmentManager().beginTransaction().replace(R.id.content, listaFragment).commit();
            getSupportFragmentManager().beginTransaction().replace(R.id.contentWeb, webViewFragment).commit();
        } else {
            getSupportFragmentManager().beginTransaction().replace(R.id.content, new RepositorioListaFragment()).commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
