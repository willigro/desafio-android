package com.rgames.githubclient.model.mvp;

import android.support.annotation.NonNull;

import com.rgames.githubclient.interfaces.GitHubPageService;
import com.rgames.githubclient.interfaces.ICallback;
import com.rgames.githubclient.interfaces.IModel;
import com.rgames.githubclient.model.basic.Repositorio;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class RepositoriosModel implements IModel.IRepositorio{

    @Override
    public void buscarRespositorios(Retrofit retrofit, String criador, String repositorio, final ICallback iCallback) {
        GitHubPageService hubPageService = retrofit.create(GitHubPageService.class);
        Call<List<Repositorio>> serviceCall = hubPageService.getRepositorioList(criador, repositorio);
        serviceCall.enqueue(new Callback<List<Repositorio>>() {
            @Override
            public void onResponse(@NonNull Call<List<Repositorio>> call, @NonNull Response<List<Repositorio>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    iCallback.onResult(response);
                } else {
                    iCallback.onResult(null);
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<Repositorio>> call, @NonNull Throwable t) {
                iCallback.onError(t);
            }
        });
    }
}
