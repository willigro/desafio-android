package com.rgames.githubclient.model.basic;

import android.os.Parcel;
import android.os.Parcelable;

public class User implements Parcelable{

    public int id;
    public String login;
    public String avatar_url;
    public String html_url;

    public User(){

    }

    protected User(Parcel in) {
        id = in.readInt();
        login = in.readString();
        avatar_url = in.readString();
        html_url = in.readString();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(login);
        parcel.writeString(avatar_url);
        parcel.writeString(html_url);
    }
}

