package com.rgames.githubclient.model.util;

import android.content.Context;

import com.rgames.githubclient.interfaces.GitHubPageService;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitUtil {

    private static Retrofit retrofit;
    private static OkHttpClient client;
    private Context mContext;

    public static Retrofit getInstance(Context context) {
        if (client == null) {
            int cacheSize = 10 * 1024 * 1024; // 10 MB
            Cache cache = new Cache(context.getCacheDir(), cacheSize);

            client = new OkHttpClient.Builder()
                    .cache(cache)
                    .addInterceptor(chain -> {
                        Request originalRequest = chain.request();
                        String cacheHeaderValue = NetworkUtil.isOnline(context)
                                ? "public, max-age=2419200"
                                : "public, only-if-cached, max-stale=2419200" ;
                        Request request = originalRequest.newBuilder().build();
                        Response response = chain.proceed(request);
                        return response.newBuilder()
                                .removeHeader("Pragma")
                                .removeHeader("Cache-Control")
                                .header("Cache-Control", cacheHeaderValue)
                                .build();
                    })
                    .addNetworkInterceptor(chain -> {
                        Request originalRequest = chain.request();
                        String cacheHeaderValue = NetworkUtil.isOnline(context)
                                ? "public, max-age=2419200"
                                : "public, only-if-cached, max-stale=2419200" ;
                        Request request = originalRequest.newBuilder().build();
                        Response response = chain.proceed(request);
                        return response.newBuilder()
                                .removeHeader("Pragma")
                                .removeHeader("Cache-Control")
                                .header("Cache-Control", cacheHeaderValue)
                                .build();
                    })
                    .build();
        }
        if (retrofit == null)
            retrofit = new Retrofit.Builder()
                    .baseUrl(GitHubPageService.BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

        return retrofit;
    }
}
