package com.rgames.githubclient.model.util;

import android.content.Context;

public class AlertDialogUtil {

    public static void showNeutral(Context context, String mensagem) {
        if (context == null)
            return;
        DialogUtil dialogUtil = new DialogUtil(context);
        dialogUtil.show();
        dialogUtil.setMensage(mensagem);
        dialogUtil.setNeutral();
    }
}
