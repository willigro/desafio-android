package com.rgames.githubclient.model.mvp;

import android.support.annotation.NonNull;

import com.rgames.githubclient.interfaces.GitHubPageService;
import com.rgames.githubclient.interfaces.ICallback;
import com.rgames.githubclient.interfaces.IModel;
import com.rgames.githubclient.model.basic.GitHubPage;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class GitHubPageModel implements IModel.IGitHubPage{
    @Override
    public void buscarPagina(Retrofit retrofit, int PAGE, final ICallback iCallback) {
        GitHubPageService hubPageService = retrofit.create(GitHubPageService.class);
        Call<GitHubPage> serviceCall = hubPageService.getGitHubPageJavaStars(PAGE);
        serviceCall.enqueue(new Callback<GitHubPage>() {
            @Override
            public void onResponse(@NonNull Call<GitHubPage> call, @NonNull Response<GitHubPage> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    iCallback.onResult(response);
                } else {
                    iCallback.onResult(null);
                }
            }

            @Override
            public void onFailure(@NonNull Call<GitHubPage> call, @NonNull Throwable t) {
               iCallback.onError(t);
            }
        });
    }
}
