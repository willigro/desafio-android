package com.rgames.githubclient.model.basic;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class GitHubPage implements Parcelable{

    public int total_count;
    public boolean incomplete_results;
    public List<Items> items;

    public GitHubPage(){
        items = new ArrayList<>();
    }

    protected GitHubPage(Parcel in) {
        total_count = in.readInt();
        incomplete_results = in.readByte() != 0;
        items = new ArrayList<>();
        in.readList(items, Items.class.getClassLoader());
    }

    public static final Creator<GitHubPage> CREATOR = new Creator<GitHubPage>() {
        @Override
        public GitHubPage createFromParcel(Parcel in) {
            return new GitHubPage(in);
        }

        @Override
        public GitHubPage[] newArray(int size) {
            return new GitHubPage[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(total_count);
        parcel.writeByte((byte) (incomplete_results ? 1 : 0));
        parcel.writeList(items);
    }
}
