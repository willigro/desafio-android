package com.rgames.githubclient.model.basic;

import android.os.Parcel;
import android.os.Parcelable;

public class Items implements Parcelable {

    public int id;
    public Owner owner;
    public String name;
    public String full_name;
    public String html_url;
    public String description;
    public int open_issues_count;
    public int forks_count;
    public int open_issues;
    public int watchers;
    public String default_branch;
    public int stargazers_count;

    public Items(){
        owner = new Owner();
    }

    protected Items(Parcel in) {
        id = in.readInt();
        name = in.readString();
        full_name = in.readString();
        html_url = in.readString();
        description = in.readString();
        open_issues_count = in.readInt();
        forks_count = in.readInt();
        open_issues = in.readInt();
        watchers = in.readInt();
        default_branch = in.readString();
        stargazers_count = in.readInt();
        owner = in.readParcelable(Owner.class.getClassLoader());
    }

    public static final Creator<Items> CREATOR = new Creator<Items>() {
        @Override
        public Items createFromParcel(Parcel in) {
            return new Items(in);
        }

        @Override
        public Items[] newArray(int size) {
            return new Items[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(name);
        parcel.writeString(full_name);
        parcel.writeString(html_url);
        parcel.writeString(description);
        parcel.writeInt(open_issues_count);
        parcel.writeInt(forks_count);
        parcel.writeInt(open_issues);
        parcel.writeInt(watchers);
        parcel.writeString(default_branch);
        parcel.writeInt(stargazers_count);
        parcel.writeParcelable(owner, i);
    }
}
