package com.rgames.githubclient.model.erros;

import java.io.IOException;

public class NoConnectivityException extends IOException {
    @Override
    public String getMessage() {
        return "Sem conexão";
    }

}
