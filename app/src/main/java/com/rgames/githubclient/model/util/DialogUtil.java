package com.rgames.githubclient.model.util;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rgames.githubclient.R;

class DialogUtil extends AlertDialog {

    private TextView txtTitle, txtMsg;
    private Button btnConfirm, btnCancel;
    private LinearLayout layContent;

    public DialogUtil(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.util_dialog);

        inicializarViews();

        if (btnCancel != null)
            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dismiss();
                }
            });
    }

    private void inicializarViews() {
        btnCancel = (Button) findViewById(R.id.btn_cancel);
        btnConfirm = (Button) findViewById(R.id.btn_confirm);
        txtTitle = (TextView) findViewById(R.id.txt_title);
        txtMsg = (TextView) findViewById(R.id.txt_msg);
        layContent = (LinearLayout) findViewById(R.id.layout_content);
    }

    @Override
    public void setTitle(@Nullable CharSequence title) {
        if (txtTitle != null)
            txtTitle.setText(title);
    }

    public void setMensage(@Nullable String msg) {
        if (txtMsg != null)
            txtMsg.setText(msg);
    }

    public void clickListenerConfirm(View.OnClickListener onClickListener) {
        if (btnConfirm != null)
            btnConfirm.setOnClickListener(onClickListener);
    }

    public void clickListenerCancel(View.OnClickListener onClickListener) {
        if (btnCancel != null)
            btnCancel.setOnClickListener(onClickListener);
    }

    public void setLayoutContent(View view) {
        if (view.getParent() != null)
            ((ViewGroup) view.getParent()).removeView(view);
        if (layContent != null)
            layContent.addView(view);
    }

    public LinearLayout getLayContent() {
        return layContent;
    }

    void setNeutral() {
        if (btnCancel != null && btnConfirm != null && layContent != null) {
            btnCancel.setVisibility(View.GONE);
            btnConfirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    layContent.setVisibility(View.GONE);
                    dismiss();
                }
            });
            btnConfirm.setText(getContext().getString(R.string.action_ok));
        }
    }
}
