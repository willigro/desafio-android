package com.rgames.githubclient.model.basic;

import android.os.Parcel;
import android.os.Parcelable;

public class Owner implements Parcelable {

    public int id;
    public String login;
    public String avatar_url;

    public Owner(){}

    protected Owner(Parcel in) {
        id = in.readInt();
        login = in.readString();
        avatar_url = in.readString();
    }

    public static final Creator<Owner> CREATOR = new Creator<Owner>() {
        @Override
        public Owner createFromParcel(Parcel in) {
            return new Owner(in);
        }

        @Override
        public Owner[] newArray(int size) {
            return new Owner[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(login);
        parcel.writeString(avatar_url);
    }
}
