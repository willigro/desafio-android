package com.rgames.githubclient.model.basic;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

public class Repositorio implements Parcelable{

    public int id;
    public String html_url;
    public String foto;
    public String title;
    public Date created_at;
    public String body;
    public User user;

    public Repositorio() {
        user = new User();
    }

    protected Repositorio(Parcel in) {
        id = in.readInt();
        html_url = in.readString();
        foto = in.readString();
        title = in.readString();
        body = in.readString();
    }

    public static final Creator<Repositorio> CREATOR = new Creator<Repositorio>() {
        @Override
        public Repositorio createFromParcel(Parcel in) {
            return new Repositorio(in);
        }

        @Override
        public Repositorio[] newArray(int size) {
            return new Repositorio[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(html_url);
        parcel.writeString(foto);
        parcel.writeString(title);
        parcel.writeString(body);
    }
}
