package com.rgames.githubclient.presenter.githubpage;

import android.content.Context;
import android.content.Intent;

import com.rgames.githubclient.interfaces.IPresenter;
import com.rgames.githubclient.model.basic.GitHubPage;
import com.rgames.githubclient.model.basic.Items;
import com.rgames.githubclient.view.githubpage.AdapterRecyclerViewGitHubPage;
import com.rgames.githubclient.view.repositorio.RepositorioActivity;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

public class AdapterGitHubPagePresenter implements IPresenter.IAdapterGitHubPage {

    private GitHubPage mGitHubgPage;
    private Context mContext;
    private RequestCreator picassoFork;
    private RequestCreator picassoEstrela;

    public AdapterGitHubPagePresenter(GitHubPage gitHubPage, Context context) {
        mGitHubgPage = gitHubPage;
        mContext = context;

        picassoFork = Picasso.with(mContext).load(
                "http://forked.yannick.io/images/logo.png"
        ).resize(40, 40);

        picassoEstrela = Picasso.with(mContext).load(
                "https://avatars0.githubusercontent.com/u/30121346?s=400&v=4"
        ).resize(40, 40);
    }

    @Override
    public void onBindViewHolder(AdapterRecyclerViewGitHubPage.GitHubPageHolder holder, int position) {
        Items items = mGitHubgPage.items.get(holder.getAdapterPosition());
        holder.txtNomeRepositorio.setText(items.name);
        holder.txtDescricaoRepositorio.setText(items.description);
        holder.txtNomeUsuario.setText(items.owner.login);
        holder.txtSobrenomeUsuario.setText("SOBRENOME DO USUARIO");
        holder.txtQuantidadeFork.setText(String.valueOf(items.forks_count));
        holder.txtQuantidadeEstrela.setText(String.valueOf(items.stargazers_count));
        clickCardOpenRepositorio(holder);
        Picasso.with(mContext).load(items.owner.avatar_url).resize(80, 80).into(holder.imgUsuario);
        picassoFork.into(holder.imgForm);
        picassoEstrela.into(holder.imgEstrela);
    }

    @Override
    public void setItens(GitHubPage gitHubPage) {
        mGitHubgPage = gitHubPage;
    }

    private void clickCardOpenRepositorio(final AdapterRecyclerViewGitHubPage.GitHubPageHolder holder) {
        holder.cardView.setOnClickListener(view -> {
            Intent intent = new Intent(mContext, RepositorioActivity.class);
            String[] args = new String[]{
                    mGitHubgPage.items.get(holder.getAdapterPosition()).owner.login
                    , mGitHubgPage.items.get(holder.getAdapterPosition()).name
            };
            intent.putExtra("OWNER", args);
            mContext.startActivity(intent);
        });
    }

    @Override
    public int getCount() {
        return (mGitHubgPage == null || mGitHubgPage.items == null) ? 0 : mGitHubgPage.items.size();
    }
}
