package com.rgames.githubclient.presenter.repositorio;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;

import com.rgames.githubclient.interfaces.ICallback;
import com.rgames.githubclient.interfaces.IModel;
import com.rgames.githubclient.interfaces.IPresenter;
import com.rgames.githubclient.interfaces.IView;
import com.rgames.githubclient.model.basic.Repositorio;
import com.rgames.githubclient.model.erros.NoConnectivityException;

import java.util.List;

import retrofit2.Response;
import retrofit2.Retrofit;

public class RepositoriosPresenter implements IPresenter.IRepositorio {

    private IModel.IRepositorio mModel;
    private IView.IRepositorio mView;

    @Override
    public void setModel(IModel.IRepositorio model) {
        mModel = model;
    }

    @Override
    public void setView(IView.IRepositorio view) {
        mView = view;
    }

    @Override
    public void toolbar(ActionBar supportActionBar, Drawable upArrow, String string) {
        if (supportActionBar != null) {
            supportActionBar.setHomeAsUpIndicator(upArrow);
            supportActionBar.setTitle((string == null) ? "Repositorios" : string);
            supportActionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void buscarRepositorios(Retrofit retrofit, Intent intent) {
        if (intent != null && intent.getStringArrayExtra("OWNER") != null) {
            String[] strings = intent.getStringArrayExtra("OWNER");
            mModel.buscarRespositorios(retrofit, strings[0], strings[1], new ICallback() {
                @Override
                public void onResult(Object object) {
                    try {
                        Response<List<Repositorio>> response = (Response<List<Repositorio>>) object;
                        if (response == null || response.body() == null) {
                            mView.finishProgress();
                            return;
                        }
                        List<Repositorio> list = response.body();
                        if (list != null && list.size() > 0)
                            mView.list(list, strings[1]);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(Throwable throwable) {
                    if (throwable instanceof NoConnectivityException)
                        mView.semConexao();
                    else
                        mView.dialog("Ocorreu um erro ao tentar estabelecer conexão com a API.");
                    mView.finishProgress();
                }
            });
        }
    }

    @Override
    public void savedInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState == null)
            mView.inicial();
        else
            mView.savedInstanceState(savedInstanceState);
    }
}
