package com.rgames.githubclient.presenter.splash;

import android.support.v7.widget.RecyclerView;

import com.rgames.githubclient.interfaces.ICallback;
import com.rgames.githubclient.interfaces.IModel;
import com.rgames.githubclient.interfaces.IPresenter;
import com.rgames.githubclient.interfaces.IView;
import com.rgames.githubclient.model.basic.GitHubPage;
import com.rgames.githubclient.model.erros.NoConnectivityException;

import retrofit2.Response;
import retrofit2.Retrofit;

public class SplashScreenPresenter implements IPresenter.ISplashScreen {
    private IView.ISplashScreen mView;
    private IModel.IGitHubPage mModel;

    public SplashScreenPresenter(IModel.IGitHubPage model) {
        mModel = model;
    }

    @Override
    public void setView(IView.ISplashScreen view) {
        mView = view;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void buscarPagina(Retrofit retrofit, int page, RecyclerView mRecyclerView) {
        mModel.buscarPagina(retrofit, page, new ICallback() {
            @Override
            public void onResult(Object object) {
                Response<GitHubPage> response = (Response<GitHubPage>) object;
                if (response == null || response.body() == null) {
                    mView.iniciarAplicacaoSemLista();
                    return;
                }
                GitHubPage gitHubPage = response.body();

                if (gitHubPage != null) {
                    if (gitHubPage.items != null && gitHubPage.items.size() > 0) {
                        mView.iniciarAplicacaoComLista(gitHubPage);
                    }
                }
            }

            @Override
            public void onError(Throwable throwable) {
                if (throwable instanceof NoConnectivityException)
                    mView.semConexao();
                else
                    mView.erro("Ocorreu um erro ao tentar estabelecer conexão com a API.");
            }
        });
    }
}
