package com.rgames.githubclient.presenter.githubpage;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;

import com.rgames.githubclient.interfaces.ICallback;
import com.rgames.githubclient.interfaces.IModel;
import com.rgames.githubclient.interfaces.IPresenter;
import com.rgames.githubclient.interfaces.IView;
import com.rgames.githubclient.model.basic.GitHubPage;
import com.rgames.githubclient.model.erros.NoConnectivityException;

import retrofit2.Response;
import retrofit2.Retrofit;

public class GitHubPagePresenter implements IPresenter.IGitHubPage {

    private IModel.IGitHubPage mModel;
    private IView.IGitHubPage mView;

    public void setView(IView.IGitHubPage mView) {
        this.mView = mView;
    }

    @Override
    public void setModel(IModel.IGitHubPage model) {
        mModel = model;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void buscarPagina(Retrofit retrofit, int page, final RecyclerView mRecyclerView) {
        mView.initProgress();
        mModel.buscarPagina(retrofit, page, new ICallback() {
            @Override
            public void onResult(Object object) {
                Response<GitHubPage> response = (Response<GitHubPage>) object;
                if (response == null || response.body() == null) {
                    mView.finishProgress();
                    return;
                }
                GitHubPage gitHubPage = response.body();

                if (gitHubPage != null) {
                    if (gitHubPage.items != null && gitHubPage.items.size() > 0) {
                        mView.listPage(gitHubPage, mRecyclerView);
                    }
                }
            }

            @Override
            public void onError(Throwable throwable) {
                mView.finishProgress();
                if (throwable instanceof NoConnectivityException)
                    mView.semConexao();
                else
                    mView.dialog("Ocorreu um erro ao tentar estabelecer conexão com a API.");
            }
        });
    }

    @Override
    public void savedInstanceState(Intent intent, Bundle savedInstanceState, RecyclerView mRecyclerView) {
        if (savedInstanceState == null) {
            if (intent != null && intent.getParcelableExtra("GIT") != null) {
                GitHubPage gitHubPage = intent.getParcelableExtra("GIT");
                mView.inicial(gitHubPage);
            } else
                mView.inicial();
        } else if (mRecyclerView != null)
            mView.savedInstanceState(savedInstanceState);
    }
}
