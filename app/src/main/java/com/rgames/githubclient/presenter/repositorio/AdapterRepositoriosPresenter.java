package com.rgames.githubclient.presenter.repositorio;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;

import com.rgames.githubclient.interfaces.IPresenter;
import com.rgames.githubclient.interfaces.IUpdateWebView;
import com.rgames.githubclient.model.basic.Repositorio;
import com.rgames.githubclient.view.repositorio.AdapterRecyclerViewRepositorios;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

public class AdapterRepositoriosPresenter implements IPresenter.IAdapterRepositorios {

    private List<Repositorio> mRepositoriosList;
    private Context mContext;
    private IUpdateWebView mIUpdateWebView;

    public AdapterRepositoriosPresenter(List<Repositorio> list, Context context, IUpdateWebView iUpdateWebView) {
        mRepositoriosList = list;
        mContext = context;
        mIUpdateWebView = iUpdateWebView;
        if (mIUpdateWebView != null)
            mIUpdateWebView.update(list.get(0).html_url);
    }

    @Override
    public void onBindViewHolder(AdapterRecyclerViewRepositorios.RepositorioViewHolder holder, int position) {
        Repositorio repositorio = mRepositoriosList.get(holder.getAdapterPosition());
        holder.txtPullTitulo.setText(repositorio.title);
        holder.txtPullDescricao.setText(repositorio.body);
        Calendar calendar = new GregorianCalendar();// Calendar.getInstance();
        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.DATE_FIELD);
        calendar.setTime(repositorio.created_at);
        dateFormat.setTimeZone(calendar.getTimeZone());
        holder.txtPullData.setText(String.valueOf(dateFormat.format(calendar.getTime())));
        holder.txtUsuarioNome.setText(repositorio.user.login);
        holder.txtUsuarioSobrenome.setText("SOBRENOME DO USUARIO");
        clickCardViewOpenRepositorioWeb(holder, repositorio);
        Picasso.with(mContext).load(repositorio.user.avatar_url).resize(80, 80).into(holder.imgUsuario);
    }

    @Override
    public void setItens(List<Repositorio> list) {
        mRepositoriosList = list;
    }

    private void clickCardViewOpenRepositorioWeb(AdapterRecyclerViewRepositorios.RepositorioViewHolder holder, final Repositorio repositorio) {
        holder.cardView.setOnClickListener(view -> {
            if (mContext.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
                mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(repositorio.html_url)));
            else if (mIUpdateWebView != null)
                mIUpdateWebView.update(repositorio.html_url);
        });
    }

    @Override
    public int getCount() {
        return (mRepositoriosList == null) ? 0 : mRepositoriosList.size();
    }
}
